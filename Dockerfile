# Use the official Python image as the base image
FROM python:3.10.0

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file into the container
COPY src/requirements.txt .

# Install the required Python packages
RUN pip install --no-cache-dir -r requirements.txt

# Copy the app code into the container
COPY . /app

# Expose the port your Flask app runs on
EXPOSE 8080

# Run the Flask app when the container starts
CMD ["python", "src/main/python/app.py"]