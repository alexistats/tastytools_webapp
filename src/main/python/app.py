from flask import Flask, request, render_template
from recipe_extractor import get_recipe_data
import os

app = Flask(__name__,
            static_folder=os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..', '..', 'assets'),
            static_url_path='/assets')
app.template_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..', '..', 'templates')

print(app.template_folder)
# Set the path to the templates folder
@app.route('/', methods=['GET', 'POST'])
def index():
    print(request.method)
    if request.method == 'POST':
        url = request.form['url']
        recipe = get_recipe_data(url)
        return render_template('recipe_template.html', recipe=recipe)
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))