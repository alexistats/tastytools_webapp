import requests
from bs4 import BeautifulSoup
import json

def fetch_html(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
        "Accept-Language": "en-US,en;q=0.9",
    }
    response = requests.get(url, headers=headers)
    response.raise_for_status()
    return response.text

def extract_field(soup, recipe_data, identifier):
    recipeElement = []
    try:
        recipeElement = recipe_data[identifier]
    except:
        ingredient_tags = soup.find_all(itemprop=identifier)
        for tag in ingredient_tags:
            recipeElement.append(tag.text.strip())
    return recipeElement

def extract_recipe(html):
    soup = BeautifulSoup(html, 'html.parser')

    # Extract metadata from JSON-LD or Microdata
    metadata = soup.find("script", {"type": "application/ld+json"}).contents
    if metadata:
        try:
            recipe_data = json.loads("".join(metadata))[0]
        except:
            recipe_data = json.loads("".join(metadata))
            if "@type" in recipe_data and recipe_data["@type"] == "Recipe":
                return recipe_data
    else:
        recipe_data = {}

    # Extract recipe name from og:title or title tag
    recipe_name = recipe_data.get('name') or soup.find('meta', property='og:title').get('content')

    # Extract recipe ingredients
    ingredients = extract_field(soup, recipe_data, 'recipeIngredient')
    instructions = extract_field(soup, recipe_data, 'recipeInstructions')


    # Extract other recipe details (e.g., servings, prep time, etc.)
    servings = extract_field(soup, recipe_data, 'recipeYield') or None
    prep_time = extract_field(soup, recipe_data, 'prepTime') or None
    cook_time = extract_field(soup, recipe_data, 'cookTime') or None
    total_time = extract_field(soup, recipe_data, 'totalTime') or None

    # Return the extracted recipe data
    recipe = {
        'name': recipe_name,
        'ingredients': ingredients,
        'instructions': instructions,
        'servings': servings,
        'prep_time': prep_time,
        'cook_time': cook_time,
        'total_time': total_time
    }
    return recipe

def get_recipe_data(url):
    html = fetch_html(url)
    recipe = extract_recipe(html)
    return recipe





# print("Recipe Name:", recipe['name'])
# print("--------------------------------------------------------------")
# print("List of ingredients")
# print("--------------------------------------------------------------")
# for ingredient in recipe['ingredients']:
#     print(ingredient)
# print("--------------------------------------------------------------")
# print("List of instructions")
# print("--------------------------------------------------------------")
# for instruction in recipe['instructions']:
#     print(instruction['text'])
# print("--------------------------------------------------------------")
# print("Servings")
# print("--------------------------------------------------------------")
# print("Servings:", recipe['servings'])
# print("--------------------------------------------------------------")
# print("Prep Time")
# print("--------------------------------------------------------------")
# print("Prep Time:", recipe['prep_time'])
# print("--------------------------------------------------------------")
# print("Cook Time")
# print("--------------------------------------------------------------")
# print("Cook Time:", recipe['cook_time'])
# print("--------------------------------------------------------------")
# print("Total Time")
# print("--------------------------------------------------------------")
# print("Total Time:", recipe['total_time'])